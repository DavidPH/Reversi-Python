import pygame as pg
import sys
from copy import deepcopy

pg.init()
pg.font.init()

font = pg.font.SysFont('Comic Sans MS', 35, True)
big_font = pg.font.SysFont('Comic Sans MS', 60, True)
black = (0, 0, 0)
white = (255, 255, 255)
grey = (56, 50, 50)
dark_green = (35, 81, 0)
light_green = (72, 168, 0)
display_width = 400
display_height = 400

pieces = [pg.image.load("black_piece.png"), pg.image.load("white_piece.png"), pg.image.load("available_move.png")]
play_img = [pg.image.load("play.png"), pg.image.load("play_pressed.png")]
show_img = pg.image.load("show.png")


gameDisplay = pg.display.set_mode((display_width, display_height))
icon = pg.image.load('logo.png')
pg.display.set_icon(icon)
pg.display.set_caption("Reversi")

gameOver = False
clock = pg.time.Clock()
no_moves_count = 0
players = []


class InputBox:
    def __init__(self, x, y, w, h, text=''):
        self.rect = pg.Rect(x, y, w, h)
        self.color = grey
        self.text = text
        self.txt_surface = font.render(text, True, self.color)
        self.active = False

    def get_text(self):
        return self.text

    def handle_event(self, event):
        if event.type == pg.MOUSEBUTTONDOWN:
            # If the user clicked on the input_box rect.
            if self.rect.collidepoint(event.pos):
                # Toggle the active variable.
                self.active = not self.active
                self.text = ''
                self.txt_surface = font.render(self.text, True, self.color)
            else:
                self.active = False

            # Change the current color of the input box.
            self.color = black if self.active else grey
        if event.type == pg.KEYDOWN:
            if self.active:
                if event.key == pg.K_RETURN:
                    self.color = grey
                    self.active = False
                    return "enter"

                elif event.key == pg.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    self.text += event.unicode
                # Re-render the text.
                self.txt_surface = font.render(self.text, True, self.color)
            if event.key == pg.K_RETURN:
                self.color = grey
                self.active = False
                return "enter"

    def update(self):
        # Resize the box if the text is too long.
        width = max(200, self.txt_surface.get_width()+10)
        self.rect.w = width

    def draw(self, screen):
        # Blit the text.
        screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
        # Blit the rect.
        pg.draw.rect(screen, self.color, self.rect, 5)


class Player:
    turn = 'B'

    def __init__(self, name, color):
        self.name = name
        self.color = color
        self.score = 2

    def score_increase(self):
        self.score += 1

    @classmethod
    def next_turn(cls):
        if cls.turn == 'B':
            cls.turn = 'W'
        else:
            cls.turn = 'B'


class Board:

    def __init__(self):
        self.rect_array = [["NA" for _ in range(8)] for _ in range(8)]
        self.piece_array = [["NA" for _ in range(8)] for _ in range(8)]
        self.piece_array[3][3] = 'W'
        self.piece_array[3][4] = 'B'
        self.piece_array[4][4] = 'W'
        self.piece_array[4][3] = 'B'

    def load_end_pos(self):
        self.piece_array = [['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']
                            , ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']
                            , ['W', 'W', 'W', 'W', 'B', 'W', 'W', 'W']
                            , ['W', 'W', 'W', 'B', 'W', 'B', 'W', 'W']
                            , ['B', 'B', 'B', 'W', 'B', 'B', 'W', 'W']
                            , ['B', 'B', 'W', 'B', 'B', 'W', 'W', 'W']
                            , ['B', 'W', 'B', 'W', 'W', 'W', 'W', 'W']
                            , ['NA', 'B', 'B', 'B', 'B', 'B', 'B', 'B']]

    def draw(self):
        color = light_green
        x = 0
        y = 0
        pg.display.update()
        for i in range(8):
            for j in range(8):
                self.rect_array[i][j] = (pg.draw.rect(gameDisplay, color, [x, y, 50, 50]))
                pg.draw.rect(gameDisplay, black, [x + 1, y + 1, 48, 48], 3)
                x += 50
                if color == dark_green:
                    color = light_green
                else:
                    color = dark_green
                if x == 400:
                    if color == dark_green:
                        color = light_green
                    else:
                        color = dark_green
                    x = 0
                    y += 50
                v_moves = show_all_valid_moves(self, Player.turn)
                if v_moves[i][j] != 'NA':
                    if v_moves[i][j] == 'W':
                        gameDisplay.blit(pieces[1], self.rect_array[i][j])
                    elif v_moves[i][j] == 'B':
                        gameDisplay.blit(pieces[0], self.rect_array[i][j])
                    elif v_moves[i][j] == 'X':
                        gameDisplay.blit(pieces[2], self.rect_array[i][j])

    def check(self, turn):
        mouse = pg.mouse.get_pos()
        for i in range(8):
            for j in range(8):
                if self.rect_array[i][j].collidepoint(mouse):
                    if self.piece_array[i][j] == "NA":
                        make_a_move(self, turn, (i, j))
                        return True
        return False


def is_valid_move(board, coordinate, turn):
    def inside_board(x, y):
        return 0 <= x <= 7 and 0 <= y <= 7

    if turn == 'W':
        not_turn = 'B'
    elif turn == 'B':
        not_turn = 'W'
    else:
        not_turn = 'error'
    pieces_to_flip = []

    for x_dir, y_dir in [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]:
        x, y = coordinate[0], coordinate[1]
        x += x_dir
        y += y_dir
        while inside_board(x, y) and board.piece_array[x][y] == not_turn:
            x += x_dir
            y += y_dir
            if inside_board(x, y) and board.piece_array[x][y] == turn:
                # Going in reverse
                while True:
                    x -= x_dir
                    y -= y_dir
                    if x == coordinate[0] and y == coordinate[1]:
                        break
                    pieces_to_flip.append([x, y])

    if len(pieces_to_flip) == 0:
        return False
    else:
        return pieces_to_flip


def flip_pieces(squares, board, turn):
    for piece in squares:
        board.piece_array[piece[0]][piece[1]] = turn


def show_all_valid_moves(board, turn):
    global no_moves_count
    global gameOver
    valid_moves = deepcopy(board.piece_array)
    no_moves = True
    for x in range(8):
        for y in range(8):
            if valid_moves[x][y] == "NA":
                if is_valid_move(board, (x, y), turn):
                    valid_moves[x][y] = 'X'
                    no_moves = False
    if no_moves:
        no_moves_count += 1
        Player.next_turn()
        if no_moves_count > 1:
            gameOver = True
        return valid_moves
    else:
        no_moves_count = 0
        return valid_moves


def make_a_move(board, turn, coordinate):
    pieces_to_flip = is_valid_move(board, coordinate, turn)

    if not pieces_to_flip:
        return False

    board.piece_array[coordinate[0]][coordinate[1]] = turn
    for x, y in pieces_to_flip:
        board.piece_array[x][y] = turn
    Player.next_turn()
    players[0].score = 0
    players[1].score = 0
    for i in range(8):
        for j in range(8):
            if board.piece_array[i][j] == players[0].color:
                players[0].score_increase()
            if board.piece_array[i][j] == players[1].color:
                players[1].score_increase()

    return True


def end_screen(board):
    show_end_screen = True
    end_surface = pg.Surface((display_width, display_height), pg.SRCALPHA)
    end_surface.fill((255, 255, 255, 0))
    translucent_surface = pg.Surface((display_width, display_height), pg.SRCALPHA)
    translucent_surface.fill((255, 255, 255, 200))
    gameDisplay.blit(translucent_surface, (0, 0))
    player_1 = font.render("{}: {}".format(players[0].name, players[0].score), True, black)
    player_2 = font.render("{}: {}".format(players[1].name, players[1].score), True, black)
    if players[0].score > players[1].score:
        winner = players[0].name
        end_surface.blit(player_1, (0, 0))
        end_surface.blit(player_2, (0, 50))
    else:
        winner = players[1].name
        end_surface.blit(player_2, (0, 0))
        end_surface.blit(player_1, (0, 50))
    if players[0].score == players[1].score:
        win_text = big_font.render("It's a tie!", True, (181, 137, 57))
    else:
        win_text = big_font.render("{} wins!".format(winner), True, (12, 155, 22))
    text_rect = win_text.get_rect()
    text_rect.center = (display_width / 2), (display_height / 3)

    # images:
    show_rect = show_img.get_rect()
    show_rect.topright = (display_width, 0)
    play_rect = play_img[0].get_rect()
    play_rect.center = (display_width//2, display_height//2 + 88)

    end_surface.blit(show_img, show_rect)
    end_surface.blit(play_img[0], play_rect)

    end_surface.blit(win_text, text_rect)
    gameDisplay.blit(end_surface, (0, 0))
    pg.display.update()

    show_board = False
    repeat = False
    while show_end_screen:

        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()

            if event.type == pg.MOUSEBUTTONDOWN:
                mouse = pg.mouse.get_pos()
                if play_rect.collidepoint(mouse):
                    end_surface.blit(play_img[1], play_rect)
                    gameDisplay.blit(end_surface, (0, 0))
                    repeat = True
            if event.type == pg.MOUSEBUTTONUP:
                mouse = pg.mouse.get_pos()
                end_surface.blit(play_img[0], play_rect)
                gameDisplay.blit(end_surface, (0, 0))
                if repeat and play_rect.collidepoint(mouse):
                    show_end_screen = False
                    game_loop()
        mouse = pg.mouse.get_pos()
        if show_rect.collidepoint(mouse):
            show_board = True
            board.draw()
        if show_board and not show_rect.collidepoint(mouse):
            show_board = False
            end_screen(board)

        pg.display.update()


def start_screen():

    def draw_empty_board():
        color = light_green
        x = 0
        y = 0
        pg.display.update()
        for i in range(8):
            for j in range(8):
                pg.draw.rect(gameDisplay, color, [x, y, 50, 50])
                pg.draw.rect(gameDisplay, black, [x + 1, y + 1, 48, 48], 3)
                x += 50
                if color == dark_green:
                    color = light_green
                else:
                    color = dark_green
                if x == 400:
                    if color == dark_green:
                        color = light_green
                    else:
                        color = dark_green
                    x = 0
                    y += 50
        translucent_surface = pg.Surface((display_width, display_height), pg.SRCALPHA)
        translucent_surface.fill((255, 255, 255, 150))
        gameDisplay.blit(translucent_surface, (0, 0))

    global players
    start = True
    draw_empty_board()

    black_piece = pg.transform.smoothscale(pieces[0], (55, 55))
    white_piece = pg.transform.smoothscale(pieces[1], (55, 55))

    play_rect = play_img[0].get_rect()
    play_rect.center = (display_width//2, display_height//2 + 88)
    gameDisplay.blit(play_img[0], play_rect)

    gameDisplay.blit(black_piece, (85, 30))
    gameDisplay.blit(white_piece, (85, 95))
    player1_entry = InputBox(150, 30, 180, 60, "Player 1")
    player2_entry = InputBox(150, 95, 180, 60, "Player 2")

    input_boxes = [player1_entry, player2_entry]
    pg.display.update()
    pressed_enter = False
    while start:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()
            for box in input_boxes:
                result = box.handle_event(event)
                if result == "enter":
                    pressed_enter = True
                box.update()
                box.draw(gameDisplay)
                gameDisplay.blit(black_piece, (85, 30))
                gameDisplay.blit(white_piece, (85, 95))
            if event.type == pg.MOUSEBUTTONDOWN or pressed_enter:
                mouse = pg.mouse.get_pos()
                if play_rect.collidepoint(mouse) or pressed_enter:
                    gameDisplay.blit(play_img[1], play_rect)
                    pg.display.update()
                    pg.time.wait(100)
            if event.type == pg.MOUSEBUTTONUP:
                mouse = pg.mouse.get_pos()
                gameDisplay.blit(play_img[0], play_rect)
                pg.display.update()
                if play_rect.collidepoint(mouse):
                    pressed_enter = True
            if pressed_enter:
                if player1_entry.get_text() != "" and player2_entry.get_text() != "":
                    players = [Player(player1_entry.get_text(), "B"), Player(player2_entry.get_text(), "W")]
                    start = False
                    game_loop()
                    pg.time.wait(80)
                else:
                    pressed_enter = False
            draw_empty_board()
            for box in input_boxes:
                box.draw(gameDisplay)
                gameDisplay.blit(black_piece, (85, 30))
                gameDisplay.blit(white_piece, (85, 95))
            gameDisplay.blit(play_img[0], play_rect)
            pg.display.update()
        clock.tick(30)


def show_score():
    score_surface = pg.Surface((display_width, 100), pg.SRCALPHA)
    score_surface.fill((255, 255, 255, 0))
    translucent_surface = pg.Surface((display_width, 100), pg.SRCALPHA)
    translucent_surface.fill((255, 255, 255, 150))
    gameDisplay.blit(translucent_surface, (0, 0))
    player_1 = font.render("{}: {}".format(players[0].name, players[0].score), True, black)
    player_2 = font.render("{}: {}".format(players[1].name, players[1].score), True, black)
    if players[0].score > players[1].score:
        score_surface.blit(player_1, (0, 0))
        score_surface.blit(player_2, (0, 50))
    else:
        score_surface.blit(player_2, (0, 0))
        score_surface.blit(player_1, (0, 50))
    gameDisplay.blit(score_surface, (0, 0))


def game_loop():
    global gameOver
    gameDisplay.fill(white)
    board = Board()
    board.draw()
    mouse_img = [pg.transform.scale(pieces[0], (30, 30)), pg.transform.scale(pieces[1], (30, 30))]
    gameOver = False
    score = False
    while not gameOver:
        board.draw()
        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()
            if event.type == pg.MOUSEBUTTONDOWN:
                board.check(Player.turn)
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_e:
                    board.load_end_pos()
                if event.key == pg.K_s:
                    score = True
            if event.type == pg.KEYUP:
                if event.key == pg.K_s:
                    score = False
        if score:
            show_score()

        m_x, m_y = pg.mouse.get_pos()
        m_x -= 15
        m_y -= 15
        if Player.turn == 'B':
            pg.mouse.set_visible(False)
            gameDisplay.blit(mouse_img[0], (m_x, m_y))
        elif Player.turn == 'W':
            pg.mouse.set_visible(False)
            gameDisplay.blit(mouse_img[1], (m_x, m_y))
        else:
            pg.mouse.set_visible(True)
        pg.display.update()
        clock.tick(30)
    board.draw()
    pg.display.update()
    pg.mouse.set_visible(True)
    end_screen(board)


start_screen()
